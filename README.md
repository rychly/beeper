# Beeper

An Android application to produce random sequences of subsequences of beeps according to predefined parameters.

By the beeps, a listener should obtain information on a corresponding sequence of numbers, e.g., to accomplish a given task based on the numbers
(such as to move to or hit particular spots, to make particular motins, to shoot particular targets, etc.).

## Parameters

* number of sequences (constant or unlimited)
* number of subsequences in each sequence (constant, or random between minimal and maximal numbers)
* number of beeps in each subsequence (constant, or random between minimal and maximal numbers)
* delay between beeps in each subsequence (constant, accelerating by a particular speed)
* delay between subsequences in each sequence (constant multiplying the previous parameter, that is the delay between beeps in each subsequence)
* delay between sequences (constant, accelerating by a particular speed, or random between minimal and maximal times)
* tone of individual beeps in each subsequence
* tone of the final beep ending each sequence

## Screen

* number of beeps in all subsequences in the current sequence (e.g., "2 3 2 1").
* number of the current sequence and number of all the sequences

## Implementation

Use [android.media.ToneGenerator](https://developer.android.com/reference/android/media/ToneGenerator) set to the maximum volume.
